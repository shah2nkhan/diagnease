﻿using DiagnEase.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DiagnEase.Domain.BO;
using DiagnEase.Infrastucture.Repositories;
using DiagnEase.ServicesCommon.Interfaces;

namespace DiagnEase.Domain.BL
{
    /// <summary>
    /// Provider will wrap repo & expose functions through Bo level object not Entity
    /// Tag Provider will wrap both individual type of tags & will expose function to read & write BO to repo
    /// </summary>
    public class TagDataProvider : ITagDataProvider
    {
        private IManualTagRepository _manualTagRepository;
        private IOpcTagRepository _opcTagRepository;

        public TagDataProvider()
        {
            _manualTagRepository=   new ManualTagRepository();
            _opcTagRepository = new OpcTagRepository();
        }
        public IEnumerable<ManualTagInfo> GetAllManualTags()
        {
            return _manualTagRepository.GetAll().Select(p => ManualTagInfo.FromEntity(p));

        }

        public IEnumerable<OpcTagInfo> GetAllOpcTags()
        {
            return _opcTagRepository.GetAll().Select(p => OpcTagInfo.FromEntity(p));
        }

        public IEnumerable<TagInfo> GetAllTags()
        {
            return GetAllOpcTags().ToList<TagInfo>().Concat(GetAllManualTags());
        }
    }
}
