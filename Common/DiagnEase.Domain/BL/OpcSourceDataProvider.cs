﻿using DiagnEase.Domain.Interfaces;
using System.Collections.Generic;
using System.Linq;
using DiagnEase.Domain.BO;
using DiagnEase.Infrastucture.Repositories;
using DiagnEase.ServicesCommon.Interfaces;

namespace DiagnEase.Domain.BL
{
    public class OpcSourceDataProvider : IOpcSourceDataProvider
    {
        private IOpcSourceRepository _OpcInfoRepository;

        public OpcSourceDataProvider()
        {
            _OpcInfoRepository = new OpcSourceRepository();
        }


        public IEnumerable<OpcSourceInfo> GetAllOpcSource()
        {
            return _OpcInfoRepository.GetAll().Select(p => OpcSourceInfo.FromEntity(p)); ;
        }
    }
}
