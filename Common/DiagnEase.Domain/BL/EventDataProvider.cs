﻿using DiagnEase.Domain.Interfaces;
using System.Collections.Generic;
using System.Linq;
using DiagnEase.Domain.BO;
using DiagnEase.Infrastucture.Repositories;
using DiagnEase.ServicesCommon.Interfaces;

namespace DiagnEase.Domain.BL
{
    public class EventDataProvider : IEventDataProvider
    {
        private IEventDataRepository _eventDataRepository;

        public EventDataProvider()
        {
            _eventDataRepository = new EventDataRepository();
        }
        public IEnumerable<EventInfo> GetAllEvents()
        {
            return _eventDataRepository.GetAll().Select(p => EventInfo.FromEntity(p)); ;
        }
    }
}
