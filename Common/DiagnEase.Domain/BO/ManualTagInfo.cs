﻿using System;
using DiagnEase.Common;
using DiagnEase.ServicesCommon.Entities;

namespace DiagnEase.Domain.BO
{
    public class ManualTagInfo : TagInfo
    {
        public double ConfiguredValue { get; set; }
        public DateTime LastUpdateTime { get; set; }
        public ManualTagInfo()
        {
            Type = TagType.Double;
        }


        internal static ManualTagInfo FromEntity(ManualTagEntity entity)
        {
            var obj = new ManualTagInfo() { ConfiguredValue = entity.ConfiguredValue, LastUpdateTime = entity.LastUpdateTime };
            TagInfo.SetFromEntity(entity, obj);
            return obj;
        }
    }
}
