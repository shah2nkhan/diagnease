﻿using DiagnEase.ServicesCommon.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiagnEase.Domain.BO
{
    public class OpcSourceInfo
    {
        public int Id { get; set; }
        public String ServerName { get; set; }
        public String IpAddress { get; set; }
        public bool Active { get; set; }

        public static OpcSourceInfo FromEntity(OpcSourceEntity entity)
        {
            return new OpcSourceInfo() { Active = entity.Active, Id = entity.Id, IpAddress = entity.IpAddress, ServerName = entity.ServerName };
        }
    }
}
