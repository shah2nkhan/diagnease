﻿using DiagnEase.Common;
using DiagnEase.ServicesCommon.Entities;

namespace DiagnEase.Domain.BO
{
    public class OpcTagInfo : TagInfo
    {
        public string PointId { get; private set; }
        public OpcTagInfo()
        {
            Type = TagType.Double;
        }

        internal static OpcTagInfo FromEntity(OpcTagEntity entity )
        {
            var obj = new OpcTagInfo() { PointId = entity.PointId };
            TagInfo.SetFromEntity(entity, obj);
            return obj;
        }
    }
}
