﻿using DiagnEase.ServicesCommon.Entities;
using System;
using DiagnEase.Common;

namespace DiagnEase.Domain.BO
{
    public abstract class TagInfo
    {
        public int Id { get; protected set; }
        public String Name { get; set; }
        public string Alias { get; set; }
        public string EngUnit { get; set; }
        public Double MinValue { get; set; }
        public Double MaxValue { get; set; }
        public string Group { get; set; }
        public TagType Type { get; set; }
        public string Description { get; set; }


        protected static void SetFromEntity(TagEntity entity , TagInfo info)
        {
            info.Id = entity.Id;
            info.Name = entity.Name;
            info.Alias = entity.Alias;
            info.EngUnit = entity.EngUnit;
            info.MinValue = entity.MinValue;
            info.MaxValue = entity.MaxValue;
            info.Group = entity.Group;
            info.Type = entity.Type;
            info.Description = entity.Description;
        }

    }
}
