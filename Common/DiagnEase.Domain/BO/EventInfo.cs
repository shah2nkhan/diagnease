﻿using DiagnEase.ServicesCommon.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiagnEase.Domain.BO
{
    public class EventInfo
    {
        public int Id { get; private set; }
        public DateTime Time { get; set; }

        public String EventTrigger { get; set; }

        public String Equipment { get; set; }

        public String Component { get; set; }

        public int Severity { get; set; }
        public String Description { get; set; }


       internal static EventInfo FromEntity(EventDataEntity entity)
        {
            return new EventInfo()
            {
                Id = entity.Id,
                Component = entity.Component,
                Description = entity.Description,
                Equipment = entity.Equipment,
                EventTrigger = entity.EventTrigger,
                Severity = entity.Severity,
                Time = entity.Time

            };
        }
    }


  
}
