﻿using DiagnEase.Domain.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiagnEase.Domain.Interfaces
{
    public interface ITagDataProvider
    {
        IEnumerable<TagInfo> GetAllTags();

        IEnumerable<OpcTagInfo> GetAllOpcTags();

        IEnumerable<ManualTagInfo> GetAllManualTags();


    }
}
