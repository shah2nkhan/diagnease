﻿using DiagnEase.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DiagnEase.ServicesCommon.Entities
{
    [Table("OPCTagTable")]
    public class OpcTagEntity : TagEntity
    {
        [MaxLength(128)]
        public string PointId { get; set; }
        public OpcTagEntity()
        {
            Type = TagType.Double;
        }

    }

}
