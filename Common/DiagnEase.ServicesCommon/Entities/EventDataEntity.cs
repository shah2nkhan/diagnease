﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DiagnEase.ServicesCommon.Entities
{
    [Table("EventTable")]
    public class EventDataEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public DateTime Time { get; set; }

        [MaxLength(256)]
        public String EventTrigger { get; set; }

        [MaxLength(256)]
        public String Equipment { get; set; }

        [MaxLength(256)]
        public String Component { get; set; }

        public int Severity { get; set; }
        [MaxLength(1024)]
        public String Description { get; set; }
    }
}
