﻿using DiagnEase.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DiagnEase.ServicesCommon.Entities
{
    [Table("ManualTagTable")]
    public class ManualTagEntity : TagEntity
    {
        public double ConfiguredValue { get; set; }
        public DateTime LastUpdateTime { get; set; }
        public ManualTagEntity()
        {
            Type = TagType.Double;
        }



    }
}
