﻿using DiagnEase.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DiagnEase.ServicesCommon.Entities
{
    [Table("TagTable")]
    public class TagEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        [MaxLength(256)]
        public String Name { get; set; }
        [MaxLength(256)]
        public string Alias { get; set; }
        [MaxLength(64)]
        public string EngUnit { get; set; }
        public Double MinValue { get; set; }
        public Double MaxValue { get; set; }
        [MaxLength(1024)]
        public string Group { get; set; }
        public TagType Type { get; set; }
        [MaxLength(1024)]
        public string Description { get; set; }

    }
}
