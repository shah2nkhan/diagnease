﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DiagnEase.ServicesCommon.Entities
{
    [Table("OpcSourceTable")]
    public class OpcSourceEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        [MaxLength(256)]
        public String ServerName { get; set; }
        [MaxLength(39)]
        public String IpAddress { get; set; }
        public bool Active { get; set; }
    }
}
