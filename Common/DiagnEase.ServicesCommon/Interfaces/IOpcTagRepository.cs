﻿using DiagnEase.ServicesCommon.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiagnEase.ServicesCommon.Interfaces
{
    public interface IOpcTagRepository :IGenericDataRepository<OpcTagEntity>
    {
    }
}
