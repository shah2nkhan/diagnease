﻿using DiagnEase.ServicesCommon.Entities;
using System.Data.Entity;

namespace DiagnEase.Infrastucture
{
    public class DiagnEaseDbContext : DbContext
    {
        public DbSet<EventDataEntity> Events { get; set; }
        public DbSet<TagEntity> TagInfos { get; set; }
        public DbSet<OpcTagEntity> OpcTagInfos { get; set; }
        public DbSet<ManualTagEntity> ManualTagInfos { get; set; }

        public DbSet<OpcSourceEntity> OpcSources { get; set; }

        public DiagnEaseDbContext():base("DiagnEase")
        {
            Database.SetInitializer<DiagnEaseDbContext>(new CreateDatabaseIfNotExists<DiagnEaseDbContext>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<ManualTagEntity>().ToTable("ManualTagTable");
            modelBuilder.Entity<OpcTagEntity>().ToTable("OPCTagTable");
        }
    }
}
