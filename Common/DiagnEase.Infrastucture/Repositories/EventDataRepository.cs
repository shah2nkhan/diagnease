﻿using DiagnEase.ServicesCommon.Entities;
using DiagnEase.ServicesCommon.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiagnEase.Infrastucture.Repositories
{
    public class EventDataRepository : GenericDataRepository<EventDataEntity>, IEventDataRepository
    {
    }
}
