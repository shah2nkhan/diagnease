namespace DiagnEase.Infrastucture.Migrations
{
    using ServicesCommon.Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DiagnEase.Infrastucture.DiagnEaseDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
        protected override void Seed(DiagnEase.Infrastucture.DiagnEaseDbContext context)
        {
            context.Events.AddOrUpdate(new EventDataEntity()
            {
                Severity = 1,
                Description = "Sample",
                Time = DateTime.Now.AddHours(-1),
                Equipment = "Unkown",
                EventTrigger = "Manual",
                Component = "Unknown"
            });


            context.Events.AddOrUpdate(new EventDataEntity()
            {
                Severity = 2,
                Description = "Sample",
                Time = DateTime.Now.AddHours(-1),
                Equipment = "Unkown",
                EventTrigger = "Manual",
                Component = "Unknown"
            });

            context.Events.AddOrUpdate(new EventDataEntity()
            {
                Severity = 3,
                Description = "Sample",
                Time = DateTime.Now.AddHours(-2),
                Equipment = "Unkown",
                EventTrigger = "Manual",
                Component = "Unknown"
            });

            context.Events.AddOrUpdate(new EventDataEntity()
            {
                Severity = 4,
                Description = "Sample",
                Time = DateTime.Now.AddHours(-3),
                Equipment = "Unkown",
                EventTrigger = "Manual",
                Component = "Unknown"
            });
            context.Events.AddOrUpdate(new EventDataEntity()
            {
                Severity = 5,
                Description = "Sample",
                Time = DateTime.Now.AddHours(-3),
                Equipment = "Unkown",
                EventTrigger = "Manual",
                Component = "Unknown"
            });

            context.SaveChanges();
        }
    }
}
