namespace DiagnEase.Infrastucture.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EventTable",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Time = c.DateTime(nullable: false),
                        EventTrigger = c.String(maxLength: 256),
                        Equipment = c.String(maxLength: 256),
                        Component = c.String(maxLength: 256),
                        Severity = c.Int(nullable: false),
                        Description = c.String(maxLength: 1024),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TagTable",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        Alias = c.String(maxLength: 256),
                        EngUnit = c.String(maxLength: 64),
                        MinValue = c.Double(nullable: false),
                        MaxValue = c.Double(nullable: false),
                        Group = c.String(maxLength: 1024),
                        Description = c.String(maxLength: 1024),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OpcSourceTable",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ServerName = c.String(maxLength: 256),
                        IpAddress = c.String(maxLength: 39),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ManualTagTable",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        ConfiguredValue = c.Double(nullable: false),
                        LastUpdateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TagTable", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.OPCTagTable",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        PointId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TagTable", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OPCTagTable", "Id", "dbo.TagTable");
            DropForeignKey("dbo.ManualTagTable", "Id", "dbo.TagTable");
            DropIndex("dbo.OPCTagTable", new[] { "Id" });
            DropIndex("dbo.ManualTagTable", new[] { "Id" });
            DropTable("dbo.OPCTagTable");
            DropTable("dbo.ManualTagTable");
            DropTable("dbo.OpcSourceTable");
            DropTable("dbo.TagTable");
            DropTable("dbo.EventTable");
        }
    }
}
