﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiagnEase.Common
{
    public class EventInfo
    {
        public DateTime EventTime { get; set; }

        //This Should Be Enum (Enum Description Attribute Should Get The String)
        public String EventTrigger { get; set; }

        public String Equipment { get; set; }

        public String ComponentDescription { get; set; }

        public String Rule { get; set; }

        public ushort Severity { get; set; }
    }
}
