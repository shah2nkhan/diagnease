﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace DiagnEase.Common
{
    public class LoggerHelper
    {
        private static ILog _logger;

        //static Initialize ()
        //{
        //    log4net.Config.XmlConfigurator.Configure();
        //    _logger = LogManager.GetLogger("DiagEase");
        //}

        public static ILog Logger
        { get { return _logger; } }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static void InitializeLogger(ILog loggger)
        {
            if(_logger == null)
            {
                _logger = Logger;
            }
        }

    }
}
