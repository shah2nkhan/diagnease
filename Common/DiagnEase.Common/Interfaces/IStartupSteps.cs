﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiagnEase.Common.Interfaces
{

    //All Initial Calls Like Loading of Data for repository Needs to inherit from This
    // Can be used for Splash Screen as well
    public interface IStartupStep
    {
        string Name { get; }
        string Description { get; }
        int Stage { get; }
        void Execute();
    }
}
