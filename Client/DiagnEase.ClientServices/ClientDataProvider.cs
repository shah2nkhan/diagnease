﻿using DiagnEase.Domain.BL;
using DiagnEase.Domain.BO;
using DiagnEase.Domain.Interfaces;
using DiagnEase.ServicesCommon.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text;

namespace DiagnEase.ClientServices
{
    /// <summary>
    /// This class will wrap every single provider and provider BO level Object by strong behaviours
    /// Using all provider
    /// 
    /// </summary>
    public class ClientDataProvider
    {
        #region Singleton Block
        static public ClientDataProvider _instance;

        public static ClientDataProvider Instance
        {
            get
            {
                return _instance;
            }
        }

        //Static Block to insure one instance  cleanest way for ThreadSafe without any lock
        //Beware it is unpredictable All dependency shoulbe available at app startup
        static ClientDataProvider()
        {
            _instance = new ClientDataProvider();
        }
        #endregion

        IEventDataProvider _eventDataProvider;
        ITagDataProvider _tagDataProvider;
        IOpcSourceDataProvider _opcSourceDataProvider;

        private ClientDataProvider()
        {
            _eventDataProvider = new EventDataProvider();
            _tagDataProvider = new TagDataProvider();
            _opcSourceDataProvider = new OpcSourceDataProvider();
        }

        public IEnumerable<EventInfo> GetAllEvents()
        {
            return _eventDataProvider.GetAllEvents(); ;
        }




        public IEnumerable<OpcTagInfo> GetAllOpcTags()
        {
            return _tagDataProvider.GetAllOpcTags();
        }

        public IEnumerable<TagInfo> GetAllTags()
        {
            return _tagDataProvider.GetAllTags();
        }

        public IEnumerable<ManualTagInfo> GetAllManualTags()
        {
            return _tagDataProvider.GetAllManualTags();

        }

        public IEnumerable<OpcSourceInfo> GetAllOpcSource()
        {
            return _opcSourceDataProvider.GetAllOpcSource();
        }



        #region AsyncFunctions
        public IObservable<IEnumerable<OpcTagInfo>> GetAllOpcTagsAsync()
        {
            return Observable.Start(GetAllOpcTags, Scheduler.Default);

        }

        public IObservable<IEnumerable<TagInfo>> GetAllTagsAsync()
        {
            return Observable.Start(GetAllTags, Scheduler.Default);

        }

        public IObservable<IEnumerable<ManualTagInfo>> GetAllManualTagsAsync()
        {
            return Observable.Start(GetAllManualTags, Scheduler.Default);
        }


        public IObservable<IEnumerable<EventInfo>> GetAllEventsAsync()
        {
            return Observable.Start(GetAllEvents, Scheduler.Default);
        }


        public IObservable<IEnumerable<OpcSourceInfo>> GetAllOpcSourceAsync()
        {
            return Observable.Start(GetAllOpcSource, Scheduler.Default);
        }
        #endregion

    }
}
