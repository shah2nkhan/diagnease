﻿using DiagnEase.UI.Controls;
using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiagnEase.UI
{
    public class DiagnEaseShellViewModel :NotificationObject
    {
        private TabItemVmDecorator _selectedComponent;

        public DiagnEaseShellViewModel(IEnumerable<TabItemVmDecorator> components )
        {
            AvailableComponent = components;
            _selectedComponent = AvailableComponent.FirstOrDefault();

        }

        public IEnumerable<TabItemVmDecorator> AvailableComponent { get; set; }

        public TabItemVmDecorator SelectedComponent
        {
            get { return _selectedComponent; }
            set
            {
                if (_selectedComponent !=value)
                {
                    _selectedComponent = value;
                    RaisePropertyChanged(() => SelectedComponent);
                }
            }
        }

    }
}
