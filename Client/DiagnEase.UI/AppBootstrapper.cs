﻿using DiagnEase.UI.Components.Configuration.ViewModels;
using DiagnEase.UI.Components.Event.ViewModels;
using DiagnEase.UI.Components.InputOutput.ViewModels;
using DiagnEase.UI.Components.RuleProcess.ViewModels;
using DiagnEase.UI.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiagnEase.UI
{
    public class AppBootstrapper
    {
        public void Run()
        {
            Initialize();

            //Need DI or Container Here ASAP
            var shellVm = new DiagnEaseShellViewModel(
                new []{
                new TabItemVmDecorator("Configuration", new ConfigurationTabMainViewModel()),
                new TabItemVmDecorator("Input / Output ", new InputOutputTabViewModel()),
                new TabItemVmDecorator("Rule Process", new RuleProcessMainTabViewModel()),
                new TabItemVmDecorator("Event List", new EventTabViewModel())});

            DiagnEaseShell shell = new DiagnEaseShell(shellVm);
            shell.Show();
        }


        //For Container Initialization Purpose Only
        private void Initialize()
        {

        }
    }

}
