﻿using DiagnEase.ClientServices;
using DiagnEase.Domain.BO;
using DiagnEase.UI.Controls;
using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;

namespace DiagnEase.UI.Components.CommonControl.TagTree
{
    public class TagTreeViewModel : BusyViewModelBase
    {
        const char TagSourceSplitter = '/';
        const string RootTagNodeHeader = "Tags";


        IEnumerable<TagSourceGroup> _tags;
        public IEnumerable<TagSourceGroup> Tags
        {
            get { return _tags; }
            set
            {
                if (_tags != value)
                {
                    _tags = value;
                    RaisePropertyChanged(() => Tags);
                }
            }
        }


        public TagTreeViewModel()
        {

        }

        public void Initialize()
        {

            
            IsBusy = true;

            ClientDataProvider.Instance.GetAllTagsAsync().
                  Finally(() =>
                  {
                      IsBusy = false;
                  })
   .ObserveOn(DispatcherScheduler).Subscribe(tags =>
   {
       tags = new[] {

           new OpcTagInfo() { Name = "Dummy1", Alias = "Dummy1", Group = "There/is/a/long/Tree/for/this/Tag" },
            new OpcTagInfo() { Name = "Dummy2", Alias = "Dummy2", Group = "There/is/a/long/Tree/for/this/Tag/Also" }

       };

       var headTag = new TagSourceGroup(RootTagNodeHeader);
       foreach (var tag in tags)
       {
           var parenttoAdd = headTag;
           if (!string.IsNullOrWhiteSpace(tag.Group))
           {
               var tagGroups = tag.Group.Split(TagSourceSplitter);
               foreach (var group in tagGroups)
               {
                   if (!string.IsNullOrWhiteSpace(group))
                   {
                       parenttoAdd = parenttoAdd.AddChildren(group);
                   }
               }

           }
           parenttoAdd.AddChildren(tag);

       }
       Tags = new[] { headTag };

   });

        }
    }
}