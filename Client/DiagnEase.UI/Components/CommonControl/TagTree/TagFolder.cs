﻿using DiagnEase.Domain.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiagnEase.UI.Components.CommonControl.TagTree
{
    //Get Tag Info Should have DFS for picking leaf nodesOnly
    public abstract class BaseTagSource
    {
        public string Name { get; set; }
        public BaseTagSource(string name)
        {
            Name = name;
        }
        public abstract bool IsLeaf { get; }
        public abstract IEnumerable<BaseTagSource> Childrens { get; }

    }

    public class TagSourceGroup : BaseTagSource
    {
        public override bool IsLeaf
        {
            get
            {
                return false; ;
            }
        }

        public override IEnumerable<BaseTagSource> Childrens
        {
            get
            {
                return _childrens;
            }
        }

        public TagSourceGroup AddChildren(string groupTag)
        {

            var data = Childrens.FirstOrDefault(p => string.Equals(p.Name, groupTag));
            if (data == null || !(data is TagSourceGroup))
            {
                var newData = new TagSourceGroup(groupTag);
                _childrens.Add(newData);
                return newData;

            }
            return data as TagSourceGroup; ;
        }

        public void AddChildren(TagInfo tag)
        {

            _childrens.Add(new TagInfoItem(tag));
        }

        private IList<BaseTagSource> _childrens;

        public TagSourceGroup(string name, IEnumerable<BaseTagSource> childrens = null) : base(name)
        {
            _childrens = new List<BaseTagSource>(childrens ?? Enumerable.Empty<BaseTagSource>());

        }
    }

    public class TagInfoItem : BaseTagSource
    {
        TagInfo _tagInfo;
        public TagInfoItem(TagInfo tagInfo) : base(string.IsNullOrWhiteSpace(tagInfo.Alias) ? tagInfo.Name : tagInfo.Alias)
        {
            _tagInfo = tagInfo;
        }

        public bool IsManualTag
        {
            get
            {
                return _tagInfo is ManualTagInfo;
            }
        }


        public TagInfo TagInfo
        {
            get { return _tagInfo; }
        }

        public override bool IsLeaf
        {
            get
            {
                return true;
            }
        }

        public override IEnumerable<BaseTagSource> Childrens
        {
            get
            {
                return Enumerable.Empty<BaseTagSource>();
            }
        }
    }
}
