﻿using DiagnEase.ClientServices;
using DiagnEase.Common;
using DiagnEase.Domain.BL;
using DiagnEase.Domain.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text;
using System.Threading;

namespace DiagnEase.UI.Components.Event.Models
{
    //For event We can use One View Model to Get the Data from DB 
    // Repository in Infrastructure  should get data from D 
    public class EventTabModel
    {
        private ClientDataProvider _dataProvider;

        public EventTabModel()
        {
            _dataProvider = ClientDataProvider.Instance;
        }

        //Just for Sample in Pricipal this may take query parameters
        public IObservable<IEnumerable<EventInfo>> GetEventAsync()
        {
            return _dataProvider.GetAllEventsAsync();
        }
    
    }

}
