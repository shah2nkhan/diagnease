﻿using DiagnEase.Common;
using DiagnEase.Domain.BO;
using DiagnEase.UI.Components.Event.Models;
using DiagnEase.UI.Controls;
using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text;
using System.Windows.Threading;

namespace DiagnEase.UI.Components.Event.ViewModels
{

    public class EventTabViewModel : BusyViewModelBase
    {
        private bool _showEvents;

        public bool ShowEvents
        {
            get { return _showEvents; }
            set
            {
                if (_showEvents != value)
                {
                    _showEvents = value;
                    GetEvents();
                    //Call The Model For data Operation
                }
            }
        }

        EventTabModel _model;
        public EventTabViewModel()
        {
            _events = new ObservableCollection<EventInfo>();
            _model = new EventTabModel();
        }

        ObservableCollection<EventInfo> _events;
        public ObservableCollection<EventInfo> Events
        {
            get { return _events; }
        }

        private void GetEvents()
        {

            IsBusy = true;
            _model.GetEventAsync().
                Finally(() => { IsBusy = false; })
                .ObserveOn(DispatcherScheduler).Subscribe(p =>
                {
                    if (p.Any())
                    {
                        //Use Bindable Colection for suppress Individual Notification
                        if (_events.Count > 0)
                            _events.Clear();
                        foreach (var item in p)
                        {
                            _events.Add(item);
                        }
                    }
                },

                exception =>
                {
                    LoggerHelper.Logger.Error("Error While Getting Events", exception);
                }
            );
        }
    }
}
