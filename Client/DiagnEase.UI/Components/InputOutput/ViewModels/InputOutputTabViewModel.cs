﻿using DiagnEase.UI.Components.CommonControl.TagTree;
using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiagnEase.UI.Components.InputOutput.ViewModels
{
    public class InputOutputTabViewModel : NotificationObject
    {

        private TagTreeViewModel _tagTreeVm;

        public TagTreeViewModel TagTreeVm
        {
            get { return _tagTreeVm; }
            set {
                if (_tagTreeVm != value)
                {
                    _tagTreeVm = value;
                    RaisePropertyChanged(() => TagTreeVm);
                }

            }
        }


        public InputOutputTabViewModel()
        {
            TagTreeVm = new TagTreeViewModel();
        }
    }
}
