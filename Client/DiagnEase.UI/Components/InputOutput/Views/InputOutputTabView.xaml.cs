﻿using DiagnEase.UI.Components.InputOutput.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DiagnEase.UI.Components.InputOutput.Views
{
    /// <summary>
    /// Interaction logic for InputOutputTabView.xaml
    /// </summary>
    public partial class InputOutputTabView : UserControl
    {
        public InputOutputTabView()
        {
            InitializeComponent();
            DataContext = new InputOutputTabViewModel();
        }
    }
}
