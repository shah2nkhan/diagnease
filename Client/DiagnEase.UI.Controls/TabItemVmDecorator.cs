﻿using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiagnEase.UI.Controls
{
    public class TabItemVmDecorator : NotificationObject
    {
        public String Header { get; set; }

        private object _contentVm;

        public object ContentVm
        {
            get { return _contentVm; }
            set {
                if (_contentVm != value)
                {
                    _contentVm = value;
                    RaisePropertyChanged(() => ContentVm);
                }

            }
        }

        public TabItemVmDecorator(string header , object contentVm)
        {
            Header = header;
            ContentVm = contentVm;
        }

    }
}
