﻿using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;

namespace DiagnEase.UI.Controls
{
    public class BusyViewModelBase : NotificationObject
    {
        //Disptacher needs to be moved to a standard Class Interface & will be Injected or passed to all.
        public static DispatcherSynchronizationContext DispatcherScheduler = new DispatcherSynchronizationContext(Dispatcher.CurrentDispatcher);

        public BusyViewModelBase()
        {

        }

        private bool _isBusy;

        public Boolean IsBusy
        {
            get
            {
                return _isBusy;
            }
            set
            {

                if (_isBusy != value)
                {
                    _isBusy = value;
                    RaisePropertyChanged(() => IsBusy);

                }
            }
        }
    }
}
